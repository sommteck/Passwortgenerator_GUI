using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Passwortgenerator_GUI
{
    public partial class Passwortgenerator : Form
    {
        private string password;            // Aufhame des generierten Passworts
        private int zaehler = 0;            // Zähler für die 8 Zeichen des Passworts
        private int zufallszahl;            // Für die Aufnahme der ermittelten Zufallszahl
        private int durchlaufszaehler = 0;  // zählt die Schleifendurchläufe

        public Passwortgenerator()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            lbl_output.Text = lbl_durchlauf.Text = txt_output.Text = null;
            txt_output.Visible = false;
            zaehler = durchlaufszaehler = 0;
            password = "";
        }

        private void cmd_password_gnerate_Click(object sender, EventArgs e)
        {
            // Objekt der Klasse Random instanzieren
            Random zufall = new Random();
            // Verarbeitungsschleife für die Generierung des achtstelligen Passworts
            while ( zaehler < 8)
            {
                // Schleifendurchlaufzähler inkrementieren
                durchlaufszaehler++;
                // Zufallszahl generieren
                // Es wird eine Zahl zwischen 35 inklusiv und 123 exklusiv generiert
                zufallszahl = zufall.Next(35, 123);
                // Verwendbare Zahlen ermitteln
                if(zufallszahl >=35 && zufallszahl <= 38 || zufallszahl >=48 && zufallszahl <=57 ||
                   zufallszahl >=64 && zufallszahl <= 90 || zufallszahl >=97 && zufallszahl <=122)
                   {
                       // Zufallszahl zu einem Zeichen konvertieren und in den String 'password' schreiben
                       password += Convert.ToChar(zufallszahl);
                       // Passwortzeichenzähler inkrementieren
                       zaehler++;
                   }
            }
            // Programmausgaben: Passwort und Anzahl Schleifendurchgänge
            txt_output.Visible = true;
            lbl_output.Text = "generiertes Passwort: "; // + password;
            txt_output.Text = password;
            lbl_durchlauf.Text = "benötigte Schleifendurchläufe:" + durchlaufszaehler.ToString();
        }
    }
}
