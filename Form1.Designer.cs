namespace Passwortgenerator_GUI
{
    partial class Passwortgenerator
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_output = new System.Windows.Forms.Label();
            this.cmd_password_gnerate = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.lbl_durchlauf = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_output.Location = new System.Drawing.Point(39, 66);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(0, 20);
            this.lbl_output.TabIndex = 0;
            // 
            // cmd_password_gnerate
            // 
            this.cmd_password_gnerate.Location = new System.Drawing.Point(24, 165);
            this.cmd_password_gnerate.Name = "cmd_password_gnerate";
            this.cmd_password_gnerate.Size = new System.Drawing.Size(150, 40);
            this.cmd_password_gnerate.TabIndex = 1;
            this.cmd_password_gnerate.Text = "&Passwort generieren";
            this.cmd_password_gnerate.UseVisualStyleBackColor = true;
            this.cmd_password_gnerate.Click += new System.EventHandler(this.cmd_password_gnerate_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(304, 165);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(78, 40);
            this.cmd_end.TabIndex = 2;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(204, 165);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(74, 40);
            this.cmd_clear.TabIndex = 3;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // lbl_durchlauf
            // 
            this.lbl_durchlauf.AutoSize = true;
            this.lbl_durchlauf.Location = new System.Drawing.Point(39, 115);
            this.lbl_durchlauf.Name = "lbl_durchlauf";
            this.lbl_durchlauf.Size = new System.Drawing.Size(0, 16);
            this.lbl_durchlauf.TabIndex = 4;
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(231, 66);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(100, 22);
            this.txt_output.TabIndex = 5;
            this.txt_output.Visible = false;
            // 
            // Passwortgenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 257);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.lbl_durchlauf);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_password_gnerate);
            this.Controls.Add(this.lbl_output);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Passwortgenerator";
            this.Text = "Passwortgenerator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.Button cmd_password_gnerate;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Label lbl_durchlauf;
        private System.Windows.Forms.TextBox txt_output;
    }
}

